public with sharing class ImageController {

  Apexpages.Standardcontroller controller;

  public ImageController() {
  }

  public ImageController(ApexPages.StandardController std) {
    this.controller = std;
  }

  public Attachment image;

  public Attachment getImage() {
    if (image == null) {
      image = new Attachment();
    }
    return image;
  }

  public void setImage(Attachment img) {
    this.image = img;
  }

  /**
   * 画像アップロード
   */
  public Pagereference uploadImage() {

    list<Image__c> dataList = new list<Image__c>();

    Image__c uploadImage = (Image__c) controller.getRecord();
    string fileName = uploadImage.image_file_name__c;

    Integer cnt = [select count() from Image__c where image_file_name__c =: fileName];
    if (cnt != 0) {
      uploadImage.image_file_name__c.addError('duplicated name.');
      image = null;
      return null;
    }

    String base64String =  EncodingUtil.base64Encode(image.Body);
    list<String> srcList = StringUtil.splitAtConstantLength(base64String, 30000);
    for (String src : srcList) {
      Image__c obj = new Image__c();
      obj.image_base64__c = src;
      obj.image_file_name__c = fileName;
      datalist.add(obj);
    }
    insert dataList;

    this.image = null;
    this.controller = null;
    uploadImage.image_file_name__c = '';
    return null;
  }

  /**
   * 画像ファイル名一覧取得
   */
  public list<String> getFileNameList() {
    List<AggregateResult> resultList = [select image_file_name__c from Image__c group by image_file_name__c];
    list<string> fileNameList = new list<string>();
    string referenceName = Image__c.image_file_name__c.getDescribe().getName();
    for (AggregateResult result : resultList) {
      fileNameList.add(String.valueOf(result.get(referenceName)));
    }
    return fileNameList;
  }

  /**
   * 画像文字列取得
   */
  @remoteAction
  public static string getImageSource(string fileName) {
    list<Image__c> imageList = [select image_base64__c from Image__c where image_file_name__c =: fileName order by Name];
    string fn = '';
    for (Image__c i : imageList) {
      fn += i.image_base64__c;
    }
    return fn;
  }
}