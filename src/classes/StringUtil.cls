public without sharing class StringUtil {

  public static List<String> splitAtConstantLength(String src, Integer splitLength) {
    list<String> result = new list<String>();
    if (src == null || splitLength == null) {
      return result;
    }

    Integer srcLength = src.length();
    if (srcLength < splitLength) {
      result.add(src);
      return result;
    }

    Decimal srcLenD = srcLength;
    Decimal splitLengthD = splitLength;
    Decimal loopTimesD = srcLenD.divide(splitLengthD, 0, System.RoundingMode.UP);
    Integer loopTimes = loopTimesD.intValue();

    Integer index = 0;
    for (integer i = 1; i <= loopTimes; i++) {
      String s = src.mid(index, splitLength);
      result.add(s);
      index = i * splitLength;
    }
    return result;
  }

}